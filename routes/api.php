<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('category/income', 'CategoryController@incomes');
Route::get('category/expense', 'CategoryController@expenses');
Route::resource('category', 'CategoryController');
Route::resource('income', 'IncomeController');
Route::resource('expense', 'ExpensesController');
Route::get('/storage/{filename}', 'ImagesController@image')->name('api.image');

Route::get('report/income/daily', 'ReportController@incomeDaily');
Route::get('report/income/weekly', 'ReportController@incomeWeekly');
Route::get('report/income/monthly', 'ReportController@incomeMonthly');
Route::get('report/expense/daily', 'ReportController@expenseDaily');
Route::get('report/expense/weekly', 'ReportController@expenseWeekly');
Route::get('report/expense/monthly', 'ReportController@expenseMonthly');