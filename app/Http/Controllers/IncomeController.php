<?php

namespace App\Http\Controllers;

use App\Income;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\IncomeRequest;

class IncomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $income = Income::orderBy('date', 'DESC')->with('category')->get();
        $returnData = [
            'status'  => '1',
            'message' => 'All Income',
            'data' => $income
        ];
        return $returnData;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IncomeRequest $request)
    {
        $input = Input::all();
        $income = Income::create($input);
        $returnData = [
            'status'  => '1',
            'message' => 'Income added',
            'data' => $income
        ];
        return $returnData;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Income  $income
     * @return \Illuminate\Http\Response
     */
    public function show(Income $income)
    {
        $returnData = [
            'status'  => '1',
            'message' => 'Single Income',
            'data' => $income
        ];
        return $returnData;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Income  $income
     * @return \Illuminate\Http\Response
     */
    public function update(IncomeRequest $request, Income $income)
    {
        $input = $request->all();
        $income->update($input);
        $returnData = [
            'status'  => '1',
            'message' => 'Income updated',
            'data' => $income
        ];
        return $returnData;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Income  $income
     * @return \Illuminate\Http\Response
     */
    public function destroy(Income $income)
    {
        $income->delete();
        $returnData = [
            'status'  => '1',
            'message' => 'Income deleted'
        ];
        return $returnData;
    }
}
