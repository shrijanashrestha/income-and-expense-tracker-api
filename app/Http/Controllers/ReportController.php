<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Income;
use App\Expense;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function __construct()
    {
        Carbon::setWeekStartsAt(Carbon::SUNDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);
    }

    public function incomeDaily()
    {
        $day = Input::get('day');
        $today = Carbon::today();
        if (empty($day))
        {
            $income = Income::where('date', $today)->orderBy('date', 'DESC')->with('category')->get();
            $date = $today;
        } else {
            $prev = Carbon::today()->subDays($day);
            $income = Income::where('date', $prev)->orderBy('date', 'DESC')->with('category')->get();
            $date = $prev;
        }
        $returnData = [
            'status'  => '1',
            'message' => 'Income Daily',
            'date' => $date,
            'data' => $income
        ];
        return $returnData;
    }

    public function incomeWeekly()
    {
        $week = Input::get('week');
        if (empty($week))
        {
            $startWeek = Carbon::now()->startOfWeek();
            $endWeek = Carbon::now()->endOfWeek();
            $income = Income::whereBetween('date', array($startWeek, $endWeek))->orderBy('date', 'DESC')->with('category')->get();
            $start = $startWeek;
            $end = $endWeek;
        } else {
            $prev = Carbon::now()->subWeeks($week);
            $prevStart = (clone $prev)->startOfWeek();
            $prevEnd = (clone $prev)->endOfWeek();
            $income = Income::whereBetween('date', array($prevStart, $prevEnd))->orderBy('date', 'DESC')->with('category')->get();
            $start = $prevStart;
            $end = $prevEnd;
        }
        $returnData = [
            'status'  => '1',
            'message' => 'Income Weekly',
            'startDate' => $start,
            'endDate' => $end,
            'data' => $income
        ];
        return $returnData;
    }

    public function incomeMonthly()
    {
        $month = Input::get('month');
        if (empty($month))
        {
            $startMonth = Carbon::now()->startOfMonth();
            $endMonth = Carbon::now()->endOfMonth();
            $income = Income::whereBetween('date', array($startMonth, $endMonth))->orderBy('date', 'DESC')->with('category')->get();
            $date = $startMonth;
        } else {
            $prev = Carbon::now()->subMonths($month);
            $prevStart = (clone $prev)->startOfMonth();
            $prevEnd = (clone $prev)->endOfMonth();
            $income = Income::whereBetween('date', array($prevStart, $prevEnd))->orderBy('date', 'DESC')->with('category')->get();
            $date = $prevStart;
        }
        $returnData = [
            'status'  => '1',
            'message' => 'Income Monthly',
            'date' => $date,
            'data' => $income
        ];
        return $returnData;
    }

    public function expenseDaily()
    {
        $day = Input::get('day');
        $today = Carbon::today();
        if (empty($day))
        {
            $expense = Expense::where('date', $today)->orderBy('date', 'DESC')->with('category')->get();
        } else {
            $prev = Carbon::today()->subDays($day);
            $expense = Expense::where('date', $prev)->orderBy('date', 'DESC')->with('category')->get();
        }
        $returnData = [
            'status'  => '1',
            'message' => 'Expense Daily',
            'data' => $expense
        ];
        return $returnData;
    }

    public function expenseWeekly()
    {
        $week = Input::get('week');
        if (empty($week))
        {
            $startWeek = Carbon::now()->startOfWeek();
            $endWeek = Carbon::now()->endOfWeek();
            $expense = Expense::whereBetween('date', array($startWeek, $endWeek))->orderBy('date', 'DESC')->with('category')->get();
        } else {
            $prev = Carbon::now()->subWeeks($week);
            $prevStart = (clone $prev)->startOfWeek();
            $prevEnd = (clone $prev)->endOfWeek();
            $expense = Expense::whereBetween('date', array($prevStart, $prevEnd))->orderBy('date', 'DESC')->with('category')->get();
        }
        $returnData = [
            'status'  => '1',
            'message' => 'Expense Weekly',
            'data' => $expense
        ];
        return $returnData;
    }

    public function expenseMonthly()
    {
        $month = Input::get('month');
        if (empty($month))
        {
            $startMonth = Carbon::now()->startOfMonth();
            $endMonth = Carbon::now()->endOfMonth();
            $expense = Expense::whereBetween('date', array($startMonth, $endMonth))->orderBy('date', 'DESC')->with('category')->get();
        } else {
            $prev = Carbon::now()->subMonths($month);
            $prevStart = (clone $prev)->startOfMonth();
            $prevEnd = (clone $prev)->endOfMonth();
            $expense = Expense::whereBetween('date', array($prevStart, $prevEnd))->orderBy('date', 'DESC')->with('category')->get();
        }
        $returnData = [
            'status'  => '1',
            'message' => 'Expense Monthly',
            'data' => $expense
        ];
        return $returnData;
    }
}
