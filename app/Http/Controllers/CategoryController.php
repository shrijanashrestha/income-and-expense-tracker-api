<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use Illuminate\Support\Facades\Input;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::orderBy('title')->get();
        $returnData = [
            'status'  => '1',
            'message' => 'All Categories',
            'data' => $category
        ];
        return $returnData;
    }

    public function incomes()
    {
        $category = Category::orderBy('title')->income()->get();
        $returnData = [
            'status'  => '1',
            'message' => 'Income Categories',
            'data' => $category
        ];
        return $returnData;
    }

    public function expenses()
    {
        $category = Category::orderBy('title')->expense()->get();
        $returnData = [
            'status'  => '1',
            'message' => 'Expense Categories',
            'data' => $category
        ];
        return $returnData;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        $input = Input::all();
        $category = Category::create($input);
        $returnData = [
            'status'  => '1',
            'message' => 'Category added',
            'data' => $category
        ];
        return $returnData;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $returnData = [
            'status'  => '1',
            'message' => 'Single Category',
            'data' => $category
        ];
        return $returnData;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        $input = $request->all();
        $category->update($input);
        $returnData = [
            'status'  => '1',
            'message' => 'Category updated',
            'data' => $category
        ];
        return $returnData;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        $returnData = [
            'status'  => '1',
            'message' => 'Category deleted'
        ];
        return $returnData;
    }
}
