<?php

namespace App\Http\Controllers;

use Storage;
use App\Expense;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests\ExpenseRequest;

class ExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expenses = Expense::orderBy('date', 'DESC')->with('category')->get();
        $returnData = [
            'status'  => '1',
            'message' => 'All Expenses',
            'data' => $expenses
        ];
        return $returnData;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ExpenseRequest $request)
    {
        $input = $request->all();
        if($request->file('image')) {
            $image = $request->file('image')->store('');
            $input['image'] = $image;
        } else {
            $input['image'] = null;
        }
        $expense = Expense::create($input);
        $returnData = [
            'status'  => '1',
            'message' => 'Expense added',
            'data' => $expense
        ];
        return $returnData;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function show(Expense $expense)
    {
        $returnData = [
            'status'  => '1',
            'message' => 'Single Expense',
            'data' => $expense
        ];
        return $returnData;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function update(ExpenseRequest $request, Expense $expense)
    {
        $input = $request->all();
        $expense->update($input);
        $returnData = [
            'status'  => '1',
            'message' => 'Expense updated',
            'data' => $expense
        ];
        return $returnData;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Expense  $expense
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expense $expense)
    {
        Storage::delete($expense->image);
        $expense->delete();
        $returnData = [
            'status'  => '1',
            'message' => 'Expense deleted'
        ];
        return $returnData;
    }
}
